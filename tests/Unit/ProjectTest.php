<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response=$this->get('/');
        
        $response->assertOk();
        //$this->assertEquals(200,$response->status());
    }
    public function testAuthorize(){
        $this->post('/login',[
            'email'=>'vladfill@mail.ru',
            'password'=>'vladfill@mail.ru'
        ]);
        $response=$this->get('unit/test');
        $response->assertOk();
    }
    public function testFailedEmailAuth(){
        $this->post('/login',[
            'email'=>'vladfillmailru',
            'password'=>'vladfill@mail.ru'
        ]);
        $response=$this->get('unit/test');
        $this->assertEquals(302,$response->status());
    }
    public function testFailedPassword(){
        $this->post('/login',[
            'email'=>'vladfillmailru',
            'password'=>'vladfills@mail.ru'
        ]);
        $response=$this->get('unit/test');
        $this->assertEquals(302,$response->status());
    }
    public function testFailedRoleAdmin(){
        $this->post('/login',[
            'email'=>'vladfill@mail.ru',
            'password'=>'vladfill@mail.ru'
        ]);
        $response=$this->get('admin');
        $this->assertEquals(403,$response->status());
    }
    public function testFailedRolesAdmin(){
        $this->post('/login',[
            'email'=>'ivan@mail.ru',
            'password'=>'ivanivan'
        ]);
        $response=$this->get('admin');
        $this->assertEquals(403,$response->status());
    }
    public function testRoleAdmin(){
        $this->post('/login',[
            'email'=>'admin@mail.ru',
            'password'=>'admin123'
        ]);
        $response=$this->get('admin');
        $this->assertEquals(200,$response->status());
    }
    public function testRoleTeacher(){
        $this->post('/login',[
            'email'=>'ivan@mail.ru',
            'password'=>'ivanivan'
        ]);
        $response=$this->get('addnews');
        $this->assertEquals(200,$response->status());
    }
    public function testFailedTeacher(){
        $this->post('/login',[
            
            'email'=>'vladfill@mail.ru',
            'password'=>'vladfill@mail.ru'
        ]);
        $response=$this->get('addnews');
        $this->assertEquals(403,$response->status());
    }
    public function testYchenik(){
        $this->post('/login',[
            
            'email'=>'vladfill@mail.ru',
            'password'=>'vladfill@mail.ru'
        ]);
        $response=$this->get('predmets');
        $this->assertEquals(200,$response->status());
    }
    public function testYchenikAccount(){
        $this->post('/login',[
            
            'email'=>'vladfill@mail.ru',
            'password'=>'vladfill@mail.ru'
        ]);
        $response=$this->get('account');
        $this->assertEquals(200,$response->status());
    }
    public function testTeacherAccount(){
        $this->post('/login',[
            'email'=>'ivan@mail.ru',
            'password'=>'ivanivan'
        ]);
        $response=$this->get('account');
        $this->assertEquals(200,$response->status());
    }
    public function testFailedYchenik(){
        $this->post('/login',[
            'email'=>'ivan@mail.ru',
            'password'=>'ivanivan'
        ]);
        $response=$this->get('predmets');
        $this->assertEquals(403,$response->status());
    }
    public function testValidateFaledStoreUser(){
        $this->post('/login',[
            'email'=>'ivan@mail.ru',
            'password'=>'ivanivan'
        ]);
        $response=$this->post('/edit',[
            'name'=>'a'
        ]);
        $this->assertEquals(302,$response->status());
    }
    
}
