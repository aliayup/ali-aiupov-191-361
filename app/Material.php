<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    public function predmets(){
        return $this->belongsTo('App\Zadaniya','z_id');
    }
}
