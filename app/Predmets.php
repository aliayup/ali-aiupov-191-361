<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Predmets extends Model
{
    public function predmets(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function zadaniya(){
        return $this->belongsTo('App\Zadaniya','predmet','predmet');
    }
   
}
