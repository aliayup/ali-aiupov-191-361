<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use App\Categor;
use App\Post;
use App\Filters\PostFilter;
class PostController extends Controller
{
    public function Submit(PostRequest $req){
        $body=$req->body;
        $name=$req->name;
        $cat=$req->category_id;
        $data = $req->all();
        if(empty($req->category_id)){
            $cat=3;
        }
        
        $filename= date('YmdHis')."-".$data['image']->getClientOriginalName();

        //Сохраняем оригинальную картинку
        $data['image']->move(Storage::path('/public/storage/image/news/').'origin/',$filename);

        //Создаем миниатюру изображения и сохраняем ее
        $thumbnail = Image::make(Storage::path('/public/storage/image/news/').'origin/'.$filename);
        $thumbnail->fit(300, 300);
        $thumbnail->save(Storage::path('/public/storage/image/news/').'thumbnail/'.$filename);

        //Сохраняем новость в БД
        $data['image'] = $filename;
        DB::table('post')->insert(['name'=>$name,'body'=>$body,'image'=>$data['image'],'cat_id'=>$cat,'created_at'=>date('Y-m-d H:i:s')]);
        return redirect()->back()->withSuccess('Успешно! Добавлена новость.');
        
    }
    public function Output(PostFilter $request){
        
        $posts=Post::orderBy('created_at', 'DESC')->filter($request)->paginate(9);
        
        $categories=Categor::orderBy('created_at', 'DESC')->get();

        return view('home', compact(['posts','categories']));
    }
    public function selectRules(){
        return view('print');
    }
}
