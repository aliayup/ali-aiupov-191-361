<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Categor;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        
        Gate::authorize('adminka',[$user]);
        $posts = Post::orderBy('created_at', 'DESC')->get();

        return view('admin.post.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        
        Gate::authorize('adminka',[$user]);
        $categories = Categor::orderBy('created_at', 'DESC')->get();

        return view('admin.post.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $body=$request->body;
        $name=$request->name;
        $cat=$request->cat_id;
        $data = $request->all();
        if(empty($request->cat_id)){
            $cat=3;
        }
        $filename= date('YmdHis')."-".$data['image']->getClientOriginalName();

        //Сохраняем оригинальную картинку
        $data['image']->move(Storage::path('/public/storage/image/news/').'origin/',$filename);

        //Создаем миниатюру изображения и сохраняем ее
        $thumbnail = Image::make(Storage::path('/public/storage/image/news/').'origin/'.$filename);
        $thumbnail->fit(300, 300);
        $thumbnail->save(Storage::path('/public/storage/image/news/').'thumbnail/'.$filename);

        //Сохраняем новость в БД
        $data['image'] = $filename;
        DB::table('post')->insert(['name'=>$name,'body'=>$body,'image'=>$data['image'],'cat_id'=>$cat,'created_at'=>date('Y-m-d H:i:s')]);

        return redirect()->back()->withSuccess('Статья была успешно обновлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post,User $user)
    {
        Gate::authorize('adminka',[$user]);
        $categories = Categor::orderBy('created_at', 'DESC')->get();

        return view('admin.post.edit', [
            'categories' => $categories,
            'post' => $post,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $data = $request->all();
        if(empty($request->cat_id)){
            $cat=3;
        }
        if(!empty($data['image'])){
        $filename= date('YmdHis')."-".$data['image']->getClientOriginalName();

        //Сохраняем оригинальную картинку
        $data['image']->move(Storage::path('/public/storage/image/news/').'origin/',$filename);

        //Создаем миниатюру изображения и сохраняем ее
        $thumbnail = Image::make(Storage::path('/public/storage/image/news/').'origin/'.$filename);
        $thumbnail->fit(300, 300);
        $thumbnail->save(Storage::path('/public/storage/image/news/').'thumbnail/'.$filename);
        $data['image'] = $filename;
        $post->image = $data['image'];
        }
        //Сохраняем новость в БД
       
        $post->name = $request->name;
        $post->body = $request->body;
        $post->cat_id = $request->cat_id;
        $post->save();

        return redirect()->back()->withSuccess('Статья была успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        
        $post->delete();
        return redirect()->back()->withSuccess('Статья была успешно удалена!');
    }
}
