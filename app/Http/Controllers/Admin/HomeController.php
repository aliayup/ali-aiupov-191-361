<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Categor;
use App\User;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    function index(User $user){
        
        Gate::authorize('adminka',[$user]);
        $posts_count = Post::all()->count();
        $categories_count = Categor::all()->count();
        $users_count = User::all()->count();
        $user_admin = User::where(
            'role_id','=','1'
            )->count();
        $user_teach = User::where(
                'role_id','=','2'
        )->count();
        $user_ychen = User::where(
                    'role_id','=','3'
        )->count();
        $users=User::all()->count();
        $pro_admin=(int)(100/$users * $user_admin+0.49);
        $pro_teach=(int)(100/$users*$user_teach+0.49);
        $pro_ychen=(int)(100/$users*$user_ychen+0.49);
        $posts_one = Post::where('cat_id','=','1')->count();
        $posts_two = Post::where('cat_id','=','2')->count();
        $posts_three = Post::where('cat_id','=','3')->count();
        $posts_four = Post::where('cat_id','=','4')->count();
        $posts_five = Post::where('cat_id','=','7')->count();
        $for_one=(int)($posts_one*100/$posts_count+0.49);
        $for_two=(int)($posts_two/$posts_count*100+0.49);
        $for_three=(int)($posts_three/$posts_count*100+0.49);
        $for_four=(int)($posts_four/$posts_count*100+0.49);
        $for_five=(int)($posts_five/$posts_count*100+0.49);


        return view('admin.home.index', [
            'posts_count' => $posts_count,
            'users_count' => $users_count,
            'categories_count' => $categories_count,
            'user_admin'=>$user_admin,
            'user_teach'=>$user_teach,
            'user_ychen'=>$user_ychen,
            'pro_admin'=>$pro_admin,
            'pro_teach'=>$pro_teach,
            'pro_ychen'=>$pro_ychen,
            'posts_one'=>$posts_one,
            'posts_two'=>$posts_two,
            'posts_three'=>$posts_three,
            'posts_four'=>$posts_four,
            'posts_five'=>$posts_five,
            'for_one'=>$for_one,
            'for_two'=>$for_two,
            'for_three'=>$for_three,
            'for_four'=>$for_four,
            'for_five'=>$for_five,
        ]);
    }
    
}
