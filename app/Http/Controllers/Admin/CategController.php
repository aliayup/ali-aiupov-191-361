<?php

namespace App\Http\Controllers\Admin;

use App\Categor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\User;
class CategController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        Gate::authorize('adminka',[$user]);
        $categories = Categor::orderBy('created_at', 'desc')->get();

        return view('admin.home.category', [
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        Gate::authorize('adminka',[$user]);
        return view('admin.home.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_category = new Categor();
        $new_category->name = $request->name;
        $new_category->save();

        return redirect()->back()->withSuccess('Категория была успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categor  $categor
     * @return \Illuminate\Http\Response
     */
    public function show(Categor $categor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categor  $categor
     * @return \Illuminate\Http\Response
     */
    public function edit(Categor $category)
    {
        
        return view('admin.home.edit', [
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categor  $categor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categor $category)
    {   
        $category->name = $request->name;
        $category->save();

        return redirect()->back()->withSuccess('Категория была успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categor  $categor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categor $category)
    {
        $category->delete();
        return redirect()->back()->withSuccess('Категория была успешно удалена!');
    }
}
