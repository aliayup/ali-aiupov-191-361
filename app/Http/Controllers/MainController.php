<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Categor;
use Auth;
use App\Http\Requests\ImageRequest;

class MainController extends Controller
{
    public function home(){
        return view('home');
    }
    public function addnews(User $user){ 
        Gate::authorize('add-post',[$user]);
        $categories=Categor::all();
        return view('addnews',compact('categories'));
    }
    public function update(ImageRequest $request,User $user){
        $data = $request->all();
        
        if(!empty($data['image'])){
        $filename= date('YmdHis')."-".$data['image']->getClientOriginalName();
        //Сохраняем оригинальную картинку
        $data['image']->move(Storage::path('/public/storage/image/news/').'origin/',$filename);

        //Создаем миниатюру изображения и сохраняем ее
        $thumbnail = Image::make(Storage::path('/public/storage/image/news/').'origin/'.$filename);
        $thumbnail->fit(300, 300);
        $thumbnail->save(Storage::path('/public/storage/image/news/').'thumbnail/'.$filename);
        $data['image'] = $filename;
        }
        
        DB::table('users')->where('id',Auth::user()->id)->update(['avatar'=>$data['image']]);
        //Сохраняем новость в БД
        return redirect()->back()->withSuccess('Профиль был успешно обновлен!');
    }
}
