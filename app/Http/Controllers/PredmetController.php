<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use App\Predmets;
use App\User;

use Illuminate\Support\Facades\Gate;

class PredmetController extends Controller
{
    public function predmets(User $user){
        Gate::authorize('view-menu',[$user]);
        $pred=DB::table('predmets')->where(
            'user_id','=',Auth::user()->id
            )->get();
        return view('predmets',compact('pred'));
    }
    public function AddRus(){
        
        if(DB::table('predmets')->where([
            ['predmet','=','Русский язык'],
            ['user_id','=',Auth::user()->id],
            ])->count()<1)
        {
             $predmet=DB::table('predmets')->insert(['user_id'=>Auth::user()->id,'predmet'=>'Русский язык','pr_id'=>'1']);

             return redirect()->back()->withSuccess('Вы записаны на курс Русского языка!');
        }
        else{
            return redirect()->back()->withSuccess('Вы уже записаны на курс Русского языка!');
        }
    }
    public function AddEng(){
        
        if(DB::table('predmets')->where([
            ['predmet','=','Английский язык'],
            ['user_id','=',Auth::user()->id],
            ])->count()<1)
        {
             $predmet=DB::table('predmets')->insert(['user_id'=>Auth::user()->id,'predmet'=>'Английский язык','pr_id'=>'2']);
             return redirect()->back()->withSuccess('Вы записаны на курс Английского языка!');
        }
        else{
            return redirect()->back()->withSuccess('Вы уже записаны на курс Английского языка!');
        }
    }
    public function AddMath(){
        
        if(DB::table('predmets')->where([
            ['predmet','=','Математика'],
            ['user_id','=',Auth::user()->id],
            ])->count()<1)
        {
             $predmet=DB::table('predmets')->insert(['user_id'=>Auth::user()->id,'predmet'=>'Математика','pr_id'=>'3']);
             return redirect()->back()->withSuccess('Вы записаны на курс по Математике!');
        }
        else{
            return redirect()->back()->withSuccess('Вы уже записаны на курс по Математике!');
        }
    }
    public function AddInform(){
        
        if(DB::table('predmets')->where([
            ['predmet','=','Информатика'],
            ['user_id','=',Auth::user()->id],
            ])->count()<1)
        {
             $predmet=DB::table('predmets')->insert(['user_id'=>Auth::user()->id,'predmet'=>'Информатика','pr_id'=>'4']);
            return redirect()->back()->withSuccess('Вы записаны на курс по Информатике!');
        }
        else{
            return redirect()->back()->withSuccess('Вы уже записаны на курс по Информатике!');
        }
    }
    public function Delete(Request $req){
        $p=$req->predmet;

        DB::table('predmets')->where([
            ['predmet','=',$p],
            ['user_id','=',Auth::user()->id],
            ])->delete();
        if($p=="Русский язык"){
        return redirect()->back()->withSuccess('Вы вышли из курса по Русскому языку!');
        }
        else if($p=="Английский язык"){
            return redirect()->back()->withSuccess('Вы вышли из курса по Английскому языку!');
        }
        else if($p=="Информатика"){
            return redirect()->back()->withSuccess('Вы вышли из курса по Информатике!');
        }
        else if($p=="Математика"){
            return redirect()->back()->withSuccess('Вы вышли из курса по Математике!');
        }
    }
   
}
