<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Zadaniya;
use App\Material;
class ZadaniyaController extends Controller
{
    public function rus(){
          
        $materials =Zadaniya::with('material')->where(
            'predmet','=','Русский язык'
            )->get();
        return view('zadaniya')->with (['materials'=>$materials]);
    }
    public function eng(){
        $materials =Zadaniya::with('material')->where(
            'predmet','=','Английский язык'
            )->get();
        return view('zadaniya')->with(['materials'=>$materials]);
    }
    public function math(){
        $materials =Zadaniya::with('material')->where(
            'predmet','=','Математика'
            )->get();
        return view('zadaniya')->with(['materials'=>$materials]);
    }
    public function inform(){
        $materials =Zadaniya::with('material')->where(
            'predmet','=','Информатика'
            )->get();
        return view('zadaniya')->with(['materials'=>$materials]);
    }
}
