<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\UserRequest;
use App\Http\Requests\EmailRequest;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    public function cabinet(){
        return view('user.cabiten');
    }
    public function updateUser(UserRequest $request, User $User){
        $name = $request->input('name');
        DB::update('update users set name = ? where id = ?',[$name,Auth::user()->id]);

        return redirect()->back()->withSuccess('Профиль был успешно обновлен!');
    }
    public function updateEmail(EmailRequest $request, User $User){
        $email = $request->input('email');
         DB::update('update users set email = ? where id = ?',[$email,Auth::user()->id]);
         
         return redirect()->back()->withSuccess('Профиль был успешно обновлен!');
    }
}
