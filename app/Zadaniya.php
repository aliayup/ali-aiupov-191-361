<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Material;
class Zadaniya extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'zadaniya';

    public function material(){
        return $this->hasOne('App\Material','z_id');
    }
}
