<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\Access\Response;
use App\User;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('add-post',function(User $user){
        if($user->role_id == 2){
            return true;
        }
    });
        Gate::define('view-menu',function(User $user){
            if($user->role_id == 3){
                return true;
            }
        });
        Gate::define('adminka',function(User $user){
            if($user->role_id == 1){
                return true;
            }
        });
    }
}
