<?php
use App\Http\Controllers\PostController;
use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix'=>'account','middleware'=>['auth']],function(){
    Route::get('/', 'UserController@cabinet');
});
Auth::routes();

Route::post('/edit', 'UserController@updateUser')->name('user_name');
Route::post('/edit-email', 'UserController@updateEmail')->name('user_email');
Route::post('/',[PostController::class,'Submit'])->name('form-input');

Route::post('/predmets/rus', 'PredmetController@AddRus')->name('rus');
Route::post('/predmets/math', 'PredmetController@AddMath')->name('math');
Route::post('/predmets/inform', 'PredmetController@AddInform')->name('inform');
Route::post('/predmets/eng', 'PredmetController@AddEng')->name('eng');
Route::post('/predmets/del/{predmet}', 'PredmetController@Delete');
Route::get('/1', 'ZadaniyaController@rus');
Route::get('/2', 'ZadaniyaController@eng');
Route::get('/3', 'ZadaniyaController@math');
Route::get('/4', 'ZadaniyaController@inform');
Auth::routes();

Route::resource('/task','ZadaniyaController');
Route::get('/predmets', 'PredmetController@predmets')->name('predm');

Route::get('/', 'PostController@Output')->name('home');
Route::get('/instruction', 'PostController@selectRules')->name('pechat');
Route::get('/addnews', 'MainController@addnews')->name('addnews');
Route::get('logout','Auth\LoginController@logout');
Route::group(['prefix'=>'admin','middleware'=>['auth']],function(){
    Route::get('/', [App\Http\Controllers\Admin\HomeController::class,'index'])->name('home_admin');
});
Route::resource('category',Admin\CategController::class);
Route::resource('post',Admin\PostsController::class);
Route::post('/send-foto','MainController@update')->name('form-foto');

Route::group(['middleware'=>'auth','prefix'=>'unit','namespace'=>'App\Http\Controllers'],function(){
    Route::get('test',function(){
        return response()->json(['ok'],200);
    });
});




