@extends('layouts.admin')
@section('title','Главная')
 
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Главная</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$posts_count}}</h3>

                <p>Количество новостей</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{route('post.index')}}" class="small-box-footer">Все статьи<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$categories_count}}</h3>

                <p>Количество категорий</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{route('category.index')}}" class="small-box-footer">Все статьи <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$users_count}}</h3>

                <p>Количество пользователей</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">Раздел пока недоступен</a>
            </div>
          </div>
        </div>
        
    <h3 class="m-0">Пользователи по категориям</h3>
        <div class="chart">
        <p class="chart__block">Администраторы - {{$user_admin}} ({{$pro_admin}}%)</p>
        <div class="pipe">
        <div style="width: {{$pro_admin}}%"> </div>
        </div>
        
        <p>Учителя - {{$user_teach}} ({{$pro_teach}}%)</p>
        <div class="pipe">
        <div style="width: {{$pro_teach}}%"> </div>
        </div>
        <p>Ученики - {{$user_ychen}} ({{$pro_ychen}}%)</p>
        <div class="pipe">
        <div style="width: {{$pro_ychen}}%"> </div>
        </div>
    </div>
    <h3 class="m-0">Новости по категориям</h3>
    <div id="graph" class="graph vertical">
          <div class="item" style="height:{{$for_one}}px">{{$posts_one}} ({{$for_one}}%)</div><p class="vertic">Достижения</p>
          <div class="item" style="height:{{$for_two}}px">{{$posts_two}} ({{$for_two}}%)</div><p class="vertic">Научные факты</p>
          <div class="item" style="height:{{$for_three}}px">{{$posts_three}} ({{$for_three}}%)</div><p class="vertic">Нет Категории</p>
          <div class="item" style="height:{{$for_four}}px">{{$posts_four}} ({{$for_four}}%)</div><p class="vertic"> Творчество</p>
          <div class="item" style="height:{{$for_five}}px">{{$posts_five}} ({{$for_five}}%)</div><p class="vertic">Полезное</p>
    </div>
  @endsection