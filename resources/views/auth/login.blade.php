@extends('layouts/app')

@section('main_content')
<div class="card">
    <div class="card__title">Вход</div>

    <div class="card__body">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-container">
                <label for="email" class="form-container__label">Ваша почта:</label>

                <div class="form-container__item">
                    <input id="email" type="email" class="form-container__input @error('email') form-container__input--miss @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <br>
                        <span class="form-container__invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-container">
                <label for="password" class="form-container__label">Пароль:</label>

                <div class="form-container__item">
                    <input id="password" type="password" class="form-container__input @error('password') form-container__input--miss @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                    <br>
                        <span class="form-container__invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-container">
                <div class="form-container__item">
                    <div class="form-container__form-check">
                        <input class="form-container__form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-container__form-check-label" for="remember">
                            Запомнить меня
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <div class="form-container__item">
                    <button type="submit" class="form-container__button">
                        Войти
                    </button>

                </div>
            </div>
        </form>
    </div>
</div>
@endsection(main_content)
