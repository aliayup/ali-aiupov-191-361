@extends('layouts/app')

@section('main_content')
<div class="card">
    <div class="card__title">Регистрация</div>

    <div class="card__body">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-container">
                <label for="name" class="form-container__label">ФИО:</label>

                <div class="form-container__item">
                    <input id="name" type="text" class="form-container__input @error('name') form-container__input--miss @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-container">
                <label for="email" class="form-container__label">E-mail</label>

                <div class="form-container__item">
                    <input id="email" type="email" class="form-container__input @error('email') form-container__input--miss @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <br>
                        <span class="form-container__invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-container">
                <label for="password" class="form-container__label">Пароль:</label>

                <div class="form-container__item">
                    <input id="password" type="password" class="form-container__input @error('password') form-container__input--miss @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                    <br>
                        <span class="form-container__invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-container">
                <label for="password-confirm" class="form-container__label">Повторите пароль:</label>

                <div class="form-container__item">
                    <input id="password-confirm" type="password" class="form-container__input" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div class="form-container">
                <div class="form-container__item">
                    <button type="submit" class="form-container__button">
                        Зарегистрироваться
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection('main_content')