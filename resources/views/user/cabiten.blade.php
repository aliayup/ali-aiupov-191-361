@extends('layouts/app')
@section('title')
Личный кабинет
@endsection('title')
@section('main_content')
@if (session('success'))
<div class="alert-container alert-container--success" role="alert">
   {{ session('success') }}
</div>
@endif
@if($errors->any())
<div class="alert-container">

   @foreach($errors->all() as $errors)
   {{$errors}}
   @endforeach

</div>
@endif
<div class="flex">
   <div class="flex-container">
      <div class="form-container">
         <form action ="{{route('user_name')}}" method = "post">
            <input  type = "hidden" name = "_token" value = "{{csrf_token()}}">
            <label for="name" class="form-container__label">Пользователь:</label>
            <div class="form-container__item">
               <input class="form-container__input" id="name" type="text" name="name" value="{{Auth::user()->name}}" required autocomplete="name" autofocus>
               <input class="form-container__button form-container__button--modif" type = 'submit' value = "ок"/>
            </div>
         </form>
      </div>

      <div class="form-container">
         <form action = "{{route('user_email')}}" method = "post">
            <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
            <label for="name" class="form-container__label">Email:</label>
            <div class="form-container__item">
               <input class="form-container__input " type = 'text' name = 'email' value = '{{Auth::user()->email}}'/>
               <input class="form-container__button form-container__button--modif" type = 'submit' value = "ок" />   
            </div>
         </form>
      </div>
      <div class="form-container">
         <label for="name" class="form-container__label">Вы:<span class="form-container__span"> {{Auth::user()->roles->name}}</span></label>
      </div>
      <div class="form-container">
         <label for="name" class="form-container__label">Первый доступ к сайту:<span class="form-container__span"> {{Auth::user()->created_at}}</span></label>
      </div>
   </div>
   
   <div class="flex-container">   
   
      @if(isset(Auth::user()->avatar))
      <div class="form-container form-container--mod">
         <div class="flex-container__image">
            <img src="/storage/storage/image/news/origin/{{Auth::user()->avatar}}">
         </div>
      </div>
      @else
      <div class="form-container form-container--mod">
         <div class="flex-container__image">
            <img src="/storage/storage/image/news/origin/user.jpg">
         </div>
      </div>
      @endif
      <form  action="{{route('form-foto')}}" method="post" enctype="multipart/form-data">
      @csrf
         <div class="form-container  form-container--mod">
            <label for="name" class="form-container__label">Добавить аватар: </label>
       
            <div class="add-file">
               <div class="add-file__item">
                  <label class="add-file__label">
                     <i class="material-icons">attach_file</i>
                     <span class="add-file__title">Добавить файл</span>
                     <input type="file"  id="image"  name="image">
                  </label>
               </div>
            </div>
            <button type="submit" class="form-container__button">Сохранить</button>
         </div>
      </form>
</div>
@endsection('main_content')