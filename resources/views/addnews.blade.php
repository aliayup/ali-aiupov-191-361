@extends('layouts/app')
@section('title')
ONschool
@endsection('title')
@section('main_content')
@if (session('success'))
<div class="alert-container alert-container--success" role="alert">
   {{ session('success') }}
</div>
@endif
@if($errors->any())
<div class="alert-container">

   @foreach($errors->all() as $errors)
   {{$errors}}
   @endforeach

</div>
@endif
<div class="card">
    <div class="card__title">Добавить новость</div>
        <div class="card__body">
            <form  action="{{route('form-input')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-container">
                    <label class="form-container__label" for="name">Название: </label>
                    <div class="form-container__item">
                        <input class="form-container__input" type="text" class="form-control" name="name">
                
                    </div>
                </div>
                <div class="form-container">
                <label class="form-container__label" for="select">Категория:
                    <div class="form-container__item">
                        <select name="category_id" class="form-container__input" aria-label=".form-select-sm example">
                                <option></option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-container">
                    <label class="form-container__label" for="body">Содержание: </label>
                    <div class="form-container__item">
                        <textarea class="form-container__input" rows="4" name="body"></textarea>
                    </div>
                </div>
                <div class="form-container form-container--modif">
                    <div class="add-file">
                        <div class="add-file__item">
                            <label class="add-file__label">
                                <i class="material-icons">attach_file</i>
                                <span class="add-file__title">Добавить файл</span>
                                <input type="file"  id="image"  name="image">
                            </label>
                        </div>
                    </div>
                </div>
                
                <button type="submit" class="form-container__button form-container__button--modific">Сохранить</button>
            </form>
        </div>
    </div>
</div>
@endsection('main_content')