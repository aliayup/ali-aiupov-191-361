@extends('layouts/app')
@section('title')
Мои Предметы
@endsection('title')
@section('main_content')
@if (session('success'))
<div class="alert-container alert-container--success" role="alert">
{{ session('success') }}
</div>
@endif

<h1 class="container__title">Мои курсы</h1>
@foreach ($pred as $predmet)
<div class="container-row-grid">  
        <a class="container-row-grid__link" href="/{{$predmet->pr_id}}">
                <div class="container-row-grid__item">
                        <label class="container-row-grid__label">{{$predmet->predmet}}</label>
                        <form action = "/predmets/del/{{$predmet->predmet}}" method = "post">
                                <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
                                <input class="container-row-grid__input" type = 'submit' value = "Выйти из курса"/>
                        </form>
                </div>
        </a>
</div>
@endforeach

<h1 class="container__title"> Записаться на курс</h1>
<div class="container-grid">
        <div class="container-grid__item">
                <form action = "{{route('rus')}}" method = "post">
                        <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
                        <input class="container-grid__input" type = 'submit' value = "Русский язык"/>
                </form>
        </div>
        <div class="container-grid__item">
                <form action = "{{route('inform')}}" method = "post">
                        <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
                        <input class="container-grid__input" type = 'submit' value = "Информатика"/>
                </form>
        </div>
        <div class="container-grid__item">
                <form action = "{{route('math')}}" method = "post">
                        <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
                        <input class="container-grid__input" type = 'submit' value = "Математика"/>
                </form>
        </div>
        <div class="container-grid__item">
                <form action = "{{route('eng')}}" method = "post">
                        <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
                        <input class="container-grid__input" type = 'submit' value = "Английский язык"/>
                </form>
        </div>
</div>

@endsection('main_content')