<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/style_home.css">
    <link rel="stylesheet" type="text/css" href="/css/auth.css">
    <link rel="stylesheet" type="text/css" href="/css/predmet.css">
    <link rel="stylesheet" type="text/css" href="/css/print.css">
    <link rel="stylesheet" href="/font/circle/stylesheet.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <div id="content">
        <header>
            <div class="header-menu" >
                <div class="top-nav" id="myTopnav">
                        <a href="{{route('home')}}" class="top-nav__link"><img src="/img/logo.png" width="80px" alt="logotip"></a>
                        @auth
                        <a href="{{url('/account')}}" class="top-nav__link">Личный кабинет</a>
                        @endauth
                        @can('view-menu')
                        <a href="{{route('predm')}}" class="top-nav__link">Предметы</a>
                        @endcan
                        @can('add-post')
                        <a href="{{route('addnews')}}" class="top-nav__link">Добавить новость</a>
                        @endcan
                        @can('adminka')
                        <a href="{{route('home_admin')}}" class="top-nav__link">Админ-Панель</a>
                        @endcan
                            @auth                                
                                <a href="{{ url('/logout') }}" class="top-nav__link top-nav__link--btn"><div class="top-nav__btn-div">Выйти</div></a>
                            @else
                                <a href="{{ route('login') }}" class="top-nav__link top-nav__link--btn"><div class="top-nav__btn-div">Войти</div></a>
                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="top-nav__link top-nav__link--btn"><div class="top-nav__btn-div">Регистрироваться</div></a>
                            @endif
                            @endauth
                
                        <a href="javascript:void(0);" class="top-nav__icon" onclick="myFunction()">
                        <i class="top-nav__icon-content icon-content"></i></a>
                </div>
            </div>
        </header>
        <main>
            <div class="wrapper">
                @yield('main_content')
            </div>
        </main>
    </div>
        <footer>
            <div class="footer">
                <div class="footer-container">
                    <div class="bottom-nav">
                        <a href="{{route('home')}}" class="bottom-nav__link bottom-nav__link--btn"><div class="bottom-nav__btn-div">Главная</div></a>  
                        <a href="{{url('/account')}}" class="bottom-nav__link bottom-nav__link--btn"><div class="bottom-nav__btn-div">Личный кабинет</div></a>
                        <a href="{{route('pechat')}}" class="bottom-nav__link bottom-nav__link--btn"><div class="bottom-nav__btn-div">О нас</div></a>
                    </div>  
                </div>
                <div class="footer-label"> 
                        ONschool Россия, Москва
                </div> 
            </div>
        </footer>
    <script  src="/js/func.js"></script>
</body>
</html>