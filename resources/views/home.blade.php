@extends('layouts/app')
@section('title')
ONschool
@endsection('title')
@section('main_content')
<div class="container">
    <h2 class="container__lavel" data-header="Школа у вас дома!">Школа у вас дома!</h2>
    <p class="container__description">Полноформатное классическое школьное образование, доступное из любой точки мира. Учитесь везде, где есть интернет!</p>
</div>
<div class="grid">
    <div class="search">
        <form action="{{route('home')}}" class="search__form" method="get">
            <p class="search__label">Поиск</p>
            <input  class="search__input" name="search_field" @if(isset($_GET['search_field'])) value="{{$_GET['search_field']}}" @endif type="text" id="search-input">
            <button class="search__button" type="submit" id="search-btn">►</button>
        </form>
    </div>
    <div class="filter">
        <form class="filter__form" action="{{route('home')}}" method="get">
                <p class="filter__label">Фильтр</p>
                <select name="category_id" class="filter__select filter__select-sm"><label for="search-input">Поиск</label>
                    <option></option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" @if(isset($_GET['category_id'])) @if($_GET['category_id'] == $category->id) selected @endif @endif>{{$category->name}}</option>
                @endforeach
                </select>
            <button type="submit" class="filter__button">►</button>
        </form>
    </div>
</div>
<div class="news">
        <h1 class="news__title">Новости</h1>
        @foreach($posts as $post)
        <div class="news__item">                            
            <div class="news__body">
                <div class="news__title">{{$post->name}}</div>
                <div class="news__date">
                    <p>Дата публикации: {{$post->created_at}}</p>          
                </div>    
                <div class="news__text">{{$post->body}}</div> 
            </div>
           <div class="news__image"><img src="/storage/storage/image/news/origin/{{ $post->image }}" alt="{{ $post->image }}"></div>
        </div>
        @endforeach 
        <div class="pagination">
        {{$posts->links('vendor.pagination.simple-default')}} 
        </div>
</div>
@endsection('main_content')