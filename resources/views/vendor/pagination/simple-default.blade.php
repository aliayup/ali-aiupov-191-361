@if ($paginator->hasPages())
    <ul class="pagination-ul" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li  class="pagination-ul__item disabled" aria-disabled="true"><span>@lang('pagination.previous')</span></li>
        @else
            <li class="pagination-ul__item"><a class="pagination__link" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pagination-ul__item"><a class="pagination__link" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a></li>
        @else
            <li class="pagination-ul__item disabled" aria-disabled="true"><span>@lang('pagination.next')</span></li>
        @endif
    </ul>
@endif
