@extends('layouts/app')
@section('title')
ONschool
@endsection('title')
@section('main_content')
@foreach($materials as $material)
     
<div class="container-row-grid"> 
     <h3>Задание id {{$material->id}}</h3>
     <div class="container-row-grid__item container-row-grid__item--hov">
          <h1 class="container-row-grid__title">
               {{$material->name}}
          </h1>
          <h4 class="container-row-grid__body">
               @if(isset($material->material))
               {{$material->material->materials}}
               @endif
          </h4>
     </div>
</div>
@endforeach     
@endsection('main_content')